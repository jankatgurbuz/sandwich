﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using System;

public class TouchController : MonoBehaviour
{
    private bool _firstposControl = true;
    private Vector3 _firstPos;
    private Transform _firstObj;
    private GameController _gameController;
    private bool _clickControl = true;

    void Start()
    {
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        LeanTouch.OnFingerSet += OnFingerSet;
        LeanTouch.OnFingerUp += OnFingerUp;
    }
    private void OnFingerUp(LeanFinger obj)
    {
        _firstposControl = true;
        _clickControl = true;
    }
    private void OnFingerSet(LeanFinger obj)
    {
        if (_gameController.GameStatus == GameStatus.Stay && _clickControl)
        {
            Ray ray = Camera.main.ScreenPointToRay(obj.ScreenPosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                // ilk elimi bastigim yerde objeyi ve ekrandaki pozisyonu aliyorum
                if (_firstposControl)
                {
                    _firstPos = obj.ScreenPosition;
                    _firstObj = hit.collider.transform;
                    _firstposControl = false;
                }

                float distance = Vector3.Distance(_firstPos, obj.ScreenPosition);
                if (distance > 5)
                {
                    Vector3 direction = ((Vector2)_firstPos - obj.ScreenPosition).normalized;
                    DirectionEnum t = Direction(Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg);
                    _clickControl = false;
                    _gameController.Move(_firstObj, t);
                }
                //Debug.DrawLine(ray.origin, hit.point, Color.red, 10);
                //Debug.Log(hit.collider.name);
            }
        } 
    }
    private DirectionEnum Direction(float angle)
    {
        angle += 180;
        if ((angle <= 45 && angle > 0) || (angle <= 360 && angle > 315))
            return DirectionEnum.Up;
        if (angle > 135 && angle <= 225)
            return DirectionEnum.Down;
        if (angle > 45 && angle <= 135)
            return DirectionEnum.Right;
        if (angle > 225 && angle <= 315)
            return DirectionEnum.Left;
        return DirectionEnum.Empty;
    }
}


