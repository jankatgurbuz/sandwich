﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;

[CreateAssetMenu(fileName = "Level", menuName = "LevelProperty", order = 1)]
public class LevelProperty : ScriptableObject
{
    [HideInInspector] public string saveStr = "";
    [SerializeField] public int row;
    [SerializeField] public int column;
    [HideInInspector] private WhichFood[,] _levelPropertyArray;
    [HideInInspector]
    public WhichFood[,] LevelPropertyArray
    {
        get
        {
            if (_levelPropertyArray == null)
            {
                string[] properyStr = saveStr.Split('-');
                _levelPropertyArray = new WhichFood[row, column];
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < column; j++)
                    {
                        int index = j + (i * column);
                        if (properyStr[index] == "NULL")
                            _levelPropertyArray[i, j] = WhichFood.Null;
                        else if (properyStr[index] == "Food")
                            _levelPropertyArray[i, j] = WhichFood.Food;
                        else if (properyStr[index] == "Bread")
                            _levelPropertyArray[i, j] = WhichFood.Bread;
                        else if (properyStr[index] == "RottenFood")
                            _levelPropertyArray[i, j] = WhichFood.RottenFood;
                    }
                }
                return _levelPropertyArray;
            }
            return _levelPropertyArray;
        }
    }
}

#if UNITY_EDITOR
[Serializable]
[CustomEditor(typeof(LevelProperty))]
public class LevelPropertyForEditor : Editor
{
    LevelProperty levelProperty;
    bool check = false;

    private void OnEnable()
    {
        levelProperty = (LevelProperty)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        levelProperty = (LevelProperty)target;
        EditorGUILayout.PropertyField(serializedObject.FindProperty("row"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("column"));
        serializedObject.FindProperty("saveStr");

        string[] options = new string[]
        {
        "NULL","Food", "Bread", "RottenFood"
        };

        if (levelProperty.saveStr == "" && levelProperty.row != 0 && levelProperty.column != 0)
        {
            for (int i = 0; i < levelProperty.row * levelProperty.column; i++)
            {
                levelProperty.saveStr += "NUll-";
            }
            levelProperty.saveStr = levelProperty.saveStr.Remove(levelProperty.saveStr.Length - 1);
        }

        int selected = 0;

        // selected = EditorGUILayout.Popup("Label", selected, options);
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        string[] strarray = levelProperty.saveStr.Split('-');
        for (int i = 0; i < levelProperty.row; i++)
        {
            EditorGUILayout.BeginHorizontal();
            for (int j = 0; j < levelProperty.column; j++)
            {
                int index = j + (i * levelProperty.column);
                selected = 0;
                if (strarray[index] == "NULL")
                {
                    selected = 0;
                }
                else if (strarray[index] == "Food")
                {
                    selected = 1;
                }
                else if (strarray[index] == "Bread")
                {
                    selected = 2;
                }
                else if (strarray[index] == "RottenFood")
                {
                    selected = 3;
                }

                selected = EditorGUILayout.Popup(selected, options);


                if (selected == 0)
                {
                    strarray[index] = "NULL";
                }
                else if (selected == 1)
                {
                    strarray[index] = "Food";
                }
                else if (selected == 2)
                {
                    strarray[index] = "Bread";
                }
                else if (selected == 3)
                {
                    strarray[index] = "RottenFood";
                }
                levelProperty.saveStr = "";
                Debug.Log(strarray.Length);
                for (int k = 0; k < levelProperty.column * levelProperty.row; k++)
                {
                    levelProperty.saveStr += strarray[k] + "-";
                }
                levelProperty.saveStr = levelProperty.saveStr.Remove(levelProperty.saveStr.Length - 1);

            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();

        serializedObject.ApplyModifiedProperties();
    }

}
#endif