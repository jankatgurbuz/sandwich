﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using TMPro;

public class GameController : MonoBehaviour
{
    #region Variable
    [HideInInspector] public GameStatus GameStatus;

    [SerializeField] private LevelProperty[] _levelPropertys;

    [SerializeField] private Grid _grid;

    [SerializeField] private GameObject _breadPrefab;
    [SerializeField] private GameObject _garbagePrefab;
    [SerializeField] private GameObject[] _foodsPrefab;
    [SerializeField] private GameObject _parentCamera;
    [SerializeField] private GameObject _sceneUI;
    [SerializeField] private GameObject _successUI;
    [SerializeField] private GameObject _failUI;

    [SerializeField] private TextMeshProUGUI _lavelNameUI;

    private Food[,] _allFood;

    private Stack<UndoController> _undoStack = new Stack<UndoController>();

    private GameObject _parentFood;

    private LevelProperty _levelProperty;

    private int saveCount = 0;
    #endregion

    #region Initialize
    private void Start()
    {
        GameStatus = GameStatus.Start;
        SaveData();
        InitCreateLevel();
        AddNeighbor();
    }
    private void SaveData()
    {
        saveCount = PlayerPrefs.GetInt("save", 0);

        if (saveCount == _levelPropertys.Length)
        {
            saveCount = 0;
            PlayerPrefs.SetInt("save", saveCount);
        }
        _lavelNameUI.text = "Level " + (saveCount + 1);
        _levelProperty = _levelPropertys[saveCount];
    }
    private void InitCreateLevel()
    {
        _parentFood = new GameObject("ParentFood");
        int row = _levelProperty.row;
        int column = _levelProperty.column;
        _allFood = new Food[row, column];
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < column; j++)
            {
                GameObject myObject = null;
                bool isBread = false;
                bool isRottenFood = false;
                switch (_levelProperty.LevelPropertyArray[i, j])
                {
                    case WhichFood.Null:
                        break;
                    case WhichFood.Food:
                        int randomFood = Random.Range(0, _foodsPrefab.Length);
                        myObject = Instantiate(_foodsPrefab[randomFood]);
                        myObject.transform.position = _grid.CellToLocal(new Vector3Int(j, 0, row - i));
                        myObject.transform.SetParent(_parentFood.transform);
                        break;
                    case WhichFood.Bread:
                        myObject = Instantiate(_breadPrefab);
                        myObject.transform.SetParent(_parentFood.transform);
                        myObject.transform.position = _grid.CellToLocal(new Vector3Int(j, 0, row - i));
                        isBread = true;
                        break;
                    case WhichFood.RottenFood:
                        myObject = Instantiate(_garbagePrefab);
                        isRottenFood = true;
                        myObject.transform.position = _grid.CellToLocal(new Vector3Int(j, 0, row - i));
                        break;
                }
                if (myObject != null)
                {
                    myObject.name = $"{i}-{j}";
                    Food f = new Food(myObject, myChildrenCount: 1, isBread, isRottenFood);
                    _allFood[i, j] = f;
                }
            }
        }
    }
    private void AddNeighbor()
    {
        for (int row = 0; row < _levelProperty.row; row++)
        {
            for (int column = 0; column < _levelProperty.column; column++)
            {
                if (_allFood[row, column] != null)
                {
                    if (NoArrayOverflow(row - 1, _levelProperty.row) && _allFood[row - 1, column] != null)
                        _allFood[row, column].Up = _allFood[row - 1, column].ThisObj;
                    else
                        _allFood[row, column].Up = null;

                    if (NoArrayOverflow(row + 1, _levelProperty.row) && _allFood[row + 1, column] != null)
                        _allFood[row, column].Down = _allFood[row + 1, column].ThisObj;
                    else
                        _allFood[row, column].Down = null;

                    if (NoArrayOverflow(column + 1, _levelProperty.column) && _allFood[row, column + 1] != null)
                        _allFood[row, column].Right = _allFood[row, column + 1].ThisObj;
                    else
                        _allFood[row, column].Right = null;

                    if (NoArrayOverflow(column - 1, _levelProperty.column) && _allFood[row, column - 1] != null)
                        _allFood[row, column].Left = _allFood[row, column - 1].ThisObj;
                    else
                        _allFood[row, column].Left = null;
                }
            }
        }
        GameStatus = GameStatus.Stay;
    }
    private bool NoArrayOverflow(int index, int arrayLength)
    {
        if (index < 0)
            return false;
        if (index == arrayLength)
            return false;
        return true;
    }
    #endregion


    public void Move(Transform movingObj, DirectionEnum directionEnum)
    {
        GameStatus = GameStatus.Swipe;
        string[] nameParse = movingObj.name.Split('-');
        GameObject obj = null;
        switch (directionEnum)
        {
            case DirectionEnum.Right:
                obj = _allFood[int.Parse(nameParse[0]), int.Parse(nameParse[1])].Right;
                MoveOperations(obj, movingObj, nameParse, new Vector2(0, -180), 0, 1);
                break;
            case DirectionEnum.Left:
                obj = _allFood[int.Parse(nameParse[0]), int.Parse(nameParse[1])].Left;
                MoveOperations(obj, movingObj, nameParse, new Vector2(0, 180), 0, -1);
                break;
            case DirectionEnum.Up:
                obj = _allFood[int.Parse(nameParse[0]), int.Parse(nameParse[1])].Up;
                MoveOperations(obj, movingObj, nameParse, new Vector2(180, 0), -1, 0);
                break;
            case DirectionEnum.Down:
                obj = _allFood[int.Parse(nameParse[0]), int.Parse(nameParse[1])].Down;
                MoveOperations(obj, movingObj, nameParse, new Vector2(-180, 0), 1, 0);
                break;
        }
        GameStatus = GameStatus.Swipe;
    }
    public void MoveOperations(GameObject myObjNeighbor, Transform movingObj,
        string[] nameParse, Vector2 rotationAngle, int rowIncrease, int columnIncrease)
    {

        if (myObjNeighbor == null)
        {
            movingObj.DOPunchRotation(new Vector3(rotationAngle.x / 18, 0, rotationAngle.y / 18), 0.5f).OnComplete(() => { GameStatus = GameStatus.Stay; });
        }
        else
        {
            Vector3 myTransform = myObjNeighbor.transform.position;
            Food myFood = _allFood[int.Parse(nameParse[0]), int.Parse(nameParse[1])];
            Food RightFood = _allFood[int.Parse(nameParse[0]) + rowIncrease, int.Parse(nameParse[1]) + columnIncrease];
            _undoStack.Push(new UndoController(
                myFood,
                RightFood,
                myFood.ThisObj.transform.position,
                rotationAngle,
                myFood.ThisObj.transform.parent,
                int.Parse(nameParse[0]),
                int.Parse(nameParse[1]),
                RightFood.MyChildrenCount,
                myFood.IsRottenFood, RightFood.IsRottenFood));

            float totalFood = (myFood.MyChildrenCount + RightFood.MyChildrenCount - 1) * 0.2f;
            movingObj.DORotate(new Vector3(rotationAngle.x, 0, rotationAngle.y), 0.4f);

            movingObj.DOJump(myTransform + new Vector3(0, totalFood, 0), 0.65f, 0, 0.5f).OnComplete(() =>
            {
                movingObj.transform.DOPunchScale(new Vector3(0.1f, 0, 0.1f), 0.2f).OnComplete(() =>
                {
                    myFood.ThisObj.transform.SetParent(RightFood.ThisObj.transform);
                    myFood.ThisObj.GetComponent<BoxCollider>().enabled = false;
                    RightFood.MyChildrenCount += myFood.MyChildrenCount;
                    _allFood[int.Parse(nameParse[0]), int.Parse(nameParse[1])] = null;
                    if (myFood.IsRottenFood)
                    {
                        RightFood.IsRottenFood = true;
                        myFood.IsRottenFood = true;
                    }
                    if (RightFood.IsRottenFood)
                    {
                        RightFood.IsRottenFood = true;
                        myFood.IsRottenFood = true;
                    }

                    AddNeighbor();
                    FinishControl(myFood);
                });
            });

        }
    }
    private void FinishControl(Food lastObj)
    {
        if (_parentFood.transform.childCount == 1)
        {
            string myObjectName = _parentFood.transform.GetChild(0).name;
            string[] myObjArray = myObjectName.Split('-');
            Food myfood = _allFood[int.Parse(myObjArray[0]), int.Parse(myObjArray[1])];

            if (myfood.IsBread && lastObj.IsBread && myfood.ThisObj.transform.Find(lastObj.ThisObj.name) != null)
            {
                if (!myfood.IsRottenFood)
                    GameStatus = GameStatus.EndWithSuccess;
                else
                    GameStatus = GameStatus.EndWithFail;
            }
        }
        else
        {
            GameStatus = GameStatus.Stay;
        }
    }


    public IEnumerator UndoEvent(bool restart, float time = 0.5f)
    {
        while (true)
        {
            if (GameStatus == GameStatus.Stay)
            {
                if (_undoStack.Count != 0)
                {
                    GameStatus = GameStatus.Undo;
                    UndoController undoCont = _undoStack.Pop();
                    undoCont.ThisFood.ThisObj.transform.SetParent(undoCont.FirstParent);
                    if (undoCont.FirstRot.y > 0)
                    {
                        undoCont.ThisFood.ThisObj.transform.DORotateQuaternion
                            (Quaternion.Euler(new Vector3(undoCont.FirstRot.x, 0, 180 + undoCont.FirstRot.y)), time - 0.1f);
                    }
                    if (undoCont.FirstRot.y < 0)
                    {
                        undoCont.ThisFood.ThisObj.transform.DORotateQuaternion
                            (Quaternion.Euler(new Vector3(undoCont.FirstRot.x, 0, undoCont.FirstRot.y - 180)), time - 0.1f);
                    }
                    if (undoCont.FirstRot.x > 0)
                    {
                        undoCont.ThisFood.ThisObj.transform.DORotateQuaternion
                            (Quaternion.Euler(new Vector3(180 + undoCont.FirstRot.x, 0, undoCont.FirstRot.y)), time - 0.1f);
                    }
                    if (undoCont.FirstRot.x < 0)
                    {
                        undoCont.ThisFood.ThisObj.transform.DORotateQuaternion
                            (Quaternion.Euler(new Vector3(undoCont.FirstRot.x - 180, 0, undoCont.FirstRot.y)), 0.4f);
                    }

                    undoCont.ThisFood.ThisObj.transform.DOJump(undoCont.FirstPos, 0.5f, 0, time).OnComplete(() =>
                    {
                    //Event OnComplete
                    undoCont.NeighborFood.MyChildrenCount = undoCont.NeighborChildCount;
                        undoCont.ThisFood.ThisObj.GetComponent<BoxCollider>().enabled = true;
                        _allFood[undoCont.Row, undoCont.Column] = undoCont.ThisFood;
                        undoCont.ThisFood.ThisObj.transform.eulerAngles = Vector3.zero;
                        undoCont.NeighborFood.IsRottenFood = undoCont.IsRottenFoodNeighbor;
                        undoCont.ThisFood.IsRottenFood = undoCont.IsRottenFood;
                        AddNeighbor();
                        GameStatus = GameStatus.Stay;
                    });
                    if (!restart)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            yield return new WaitForSeconds(0.01f);
        }
    }

    #region ButtonClicks
    public void UndoButtonClick()
    {
        StartCoroutine(UndoEvent(false));
    }
    public void RestartButtonClick()
    {
        StartCoroutine(UndoEvent(true, 0.25f));
    }
    public void FailButton()
    {
        SceneManager.LoadScene(0);
    }
    public void SuccessButton()
    {
        PlayerPrefs.SetInt("save", ++saveCount);
        SceneManager.LoadScene(0);
    }
    #endregion

    private void FixedUpdate()
    {
        switch (GameStatus)
        {
            case GameStatus.EndWithSuccess:
                Camera.main.transform.DOMoveY(2f, 3);
                Camera.main.GetComponent<Animator>().enabled = true;
                Camera.main.transform.DOLookAt(_parentFood.transform.GetChild(0).position + Vector3.up * 3, 3f);
                _sceneUI.SetActive(false);
                _successUI.SetActive(true);
                GameStatus = GameStatus.End;
                break;
            case GameStatus.EndWithFail:
                Camera.main.transform.DOMoveY(2f, 3);
                Camera.main.GetComponent<Animator>().enabled = true;
                Camera.main.transform.DOLookAt(_parentFood.transform.GetChild(0).position + Vector3.up * 3, 3f);
                _sceneUI.SetActive(false);
                _failUI.SetActive(true);
                GameStatus = GameStatus.End;
                break;
        }
    }
}
