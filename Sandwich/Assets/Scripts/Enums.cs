﻿
public enum WhichFood
{
    Null,
    Food,
    Bread,
    RottenFood
}


public enum DirectionEnum
{
    Empty,
    Right,
    Left,
    Up,
    Down
}

public enum GameStatus
{
    Start,
    Stay,
    Swipe,
    Undo,
    EndWithSuccess,
    EndWithFail,
    End
}