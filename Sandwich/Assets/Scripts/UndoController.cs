﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndoController
{
    public Food ThisFood;
    public Food NeighborFood;
    public Vector3 FirstPos;
    public Vector3 FirstRot;
    public Transform FirstParent;
    public int Row, Column;
    public int NeighborChildCount;
    public bool IsRottenFood;
    public bool IsRottenFoodNeighbor;


    public UndoController(Food thisFood,
        Food neighborFood,
        Vector3 firstPos,
        Vector3 firstRot,
        Transform firstParent,
        int row, int column, int neighborChildCount,
        bool isRottenFood, bool isRottenFoodNeighbor)
    {
        ThisFood = thisFood;
        NeighborFood = neighborFood;
        FirstPos = firstPos;
        FirstRot = firstRot;
        FirstParent = firstParent;
        Row = row;
        Column = column;
        NeighborChildCount = neighborChildCount;
        IsRottenFood = isRottenFood;
        IsRottenFoodNeighbor = isRottenFoodNeighbor;
    }
}
