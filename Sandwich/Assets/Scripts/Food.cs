﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food 
{
    public GameObject Up, Right, Left, Down;
    public GameObject ThisObj;
    public int Row, Column;
    public int MyChildrenCount;
    public bool IsBread;
    public bool IsRottenFood;
    public Food(GameObject thisObj,int myChildrenCount,  bool isBread,bool isRottenFood)
    {
        ThisObj = thisObj;
        MyChildrenCount = myChildrenCount;
        IsBread = isBread;
        IsRottenFood = isRottenFood;
    }
}
